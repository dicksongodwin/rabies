<?php

namespace App;


use Illuminate\Database\Eloquent\Model;

class OdkModel extends Model {
    protected $connection = 'odk';
    protected $primaryKey = '_URI';
    public $timestamps = false;
    public $incrementing = false;

    public function newQuery() {
        $query = parent::newQuery();
        return $query->orderBy('_CREATION_DATE', 'desc');
    }

    public function user() {
        //todo: cant use eloquent relations across multiple connections
        return User::find($this->USERNAME);
    }

    public static function getShortCode() {
        return with(new static)->shortCode;
    }

    public  function getDataSender() {
        if ($this->user()) {
            return $this->user()->fullname;
        }

        $nameFromId = $this->getDataSenderFromInstanceId();
        if ($nameFromId) {
            return $nameFromId;
        } else {
          return '-';
        }
    }


    private function getDataSenderFromInstanceId() {

        $instance = Instance::find($this->getUri());

        if (!$instance) return null;

        if (!$instance->user) return null;

        return '*'.$instance->user->fullname;
    }


    public static function getTableName() {
        return with(new static)->getTable();
    }

    public  function getDataSenderFacility() {
        //todo: optimize user
        if ($this->user()) {
            $f = $this->user()->facilities;
            if (sizeof($f) > 0) {
                return $f[0]->name;
            }

            return 'Unknown';
        } else {
            return 'Unknown';
        }
    }

    public function getUri() {
        return $this->_URI;
    }

    public function getFormDataModel() {
        $m = OdkFormModel::where('PERSIST_AS_TABLE_NAME', self::getTableName())
            ->select('ELEMENT_NAME', 'PERSIST_AS_COLUMN_NAME')
            ->get();

        $mms = [];
        foreach ($m as $item) {

            if ($item->PERSIST_AS_COLUMN_NAME == "") continue;
            if ($item->PERSIST_AS_COLUMN_NAME == "META_INSTANCE_ID") continue;

            $mms[$item->PERSIST_AS_COLUMN_NAME] = strtoupper($item->ELEMENT_NAME);
        }

        return $mms;
    }

    public static function  getFormDataModelStatic() {
        $m = OdkFormModel::where('PERSIST_AS_TABLE_NAME', self::getTableName())
            ->select('ELEMENT_NAME', 'PERSIST_AS_COLUMN_NAME')
            ->get();

        $mms = [];
        foreach ($m as $item) {

            if ($item->PERSIST_AS_COLUMN_NAME == "") continue;
            if ($item->PERSIST_AS_COLUMN_NAME == "META_INSTANCE_ID") continue;

            $mms[$item->PERSIST_AS_COLUMN_NAME] = strtoupper($item->ELEMENT_NAME);
        }

        return $mms;
    }

}
