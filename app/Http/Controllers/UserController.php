<?php

namespace App\Http\Controllers;

use App\Models\Facility;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Khill\Lavacharts\Lavacharts;

class UserController extends Controller {

    public function __construct() {
        $this->middleware('auth', ['except' => ['profile', 'apiLogin', 'updateFcmToken']]);
    }

    public function getLogin() {
        return view('auth/login');
    }

    public function logout() {
        Auth::logout();
        return redirect('/home');
    }

    public function apiLogin(Request $request) {

        $username = $request->get('username', '');
        $password = $request->get('password', '');

        //dd($request->all());

        $user = User::where('username', $username)
            ->where('password', $password)
            ->first();

        //dd($user);


        if ($user) {

            //automatically injects this into $user
            $user->formIds = $user->forms();
            $user->districts;

            if ($user->role === 2) {
                $user->facilities = $user->getDistrictAsFacilityHack();
            } else {
                $user->facilities;
            }

            return response()->json([
                'success' => true,
                'user' => $user
            ]);

        } else {

            return response()->json([
                'success' => false,
                'message' => 'User does not exist'
            ]);
        }
    }

    public function updateFcmToken(Request $request) {
        $user = User::find($request->get('userId'));
        $user->fcm_token = $request->get('newToken');
        $s = $user->save();

        return response()->json([
            'success' => $s
        ]);

    }

    public function profile($id) {

        $dbData = DB::connection('odk')->select("CALL get_stats_rabies_user(1, 60, $id)");
        $dbData = array_reverse($dbData);
        $dbData = collect($dbData)->map(function($item) {
            return [ Carbon::parse($item->dt)->format('d M, Y'), $item->visits + $item->obstetrics + $item->postnatal];
        });

        $legendSuffix = 'Submissions for last  '. sizeof($dbData)  .' days';

        $lava = new Lavacharts;

        $casesDataTable1 = $lava->DataTable()->addDateColumn('Date')->addNumberColumn('Submissions'); // . $legendSuffix);

        foreach ($dbData as $datum) {
            $casesDataTable1->addRow([$datum[0],  $datum[1]]);
        }

        $options = [
            'title' => $legendSuffix,
            'legend' => 'top',
            'hAxis' => array(
                'baselineColor' => '#f2f2f2',
                'gridlineColor' => '#f0f2f2'
            ),
            'animation' => array(
                'duration' => 600,
                'startup' => true
            )
        ];

        $lava->LineChart('Cases1', $casesDataTable1, $options);


        return view('user-app-profile')->with('user', User::find($id))->with('lavaObject', $lava);
    }

    public function register() {
        $facilities = \App\Models\Facility::orderBy('name', 'asc')->get();
        $districts = \App\Models\District::orderBy('name', 'asc')->get();

        return view('register')
            ->with('facilities', $facilities)
            ->with('ds', $districts);
    }

    public function uaf($id) {
        $user = User::find($id);
        $facilities = $user->facilities;
        $districts = $user->districts;


        return view('uaf')
            ->with('user', $user)
            ->with('facilities', $facilities)
            ->with('districts', $districts);
    }

    public function registerFacility(Request $request) {

        $fac = new Facility();
        $fac->name = $request->get('fac');
        $fac->district = $request->get('districtId');
        $facSaved = $fac->save();

        if ($facSaved){
            echo '<h1>Success</h1>';
            echo '<a href="/register">Add more users/facilities</a>';
            echo '<pre>',print_r($fac->toJson(JSON_PRETTY_PRINT)),'</pre>';
        }

    }

    public function registerUser(Request $request) {

        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255|min:6',
            'username' => 'required|max:255|min:3',
            'email' => 'email',
            'phone' => 'required|min:9|max:12',
            'role' => 'required|numeric',
            'password' => 'required|min:4',
            'facility' => 'required|numeric'
        ]);

        if ($validator->fails()) {
            echo '<h1>Error</h1>';
            echo '<pre>',print_r($validator->errors()->toJson(JSON_PRETTY_PRINT)),'</pre>';
        }

        //validation passed
        $user = new User();
        $user->fullname = $request->get('fullname');
        $user->phone = $request->get('phone');
        $user->username = $request->get('username');
        $user->password = $request->get('password');
        $user->email = $request->get('email');
        $user->role = $request->get('role');
        $userSaved = $user->save();

        if ($userSaved) {

            if ((int)$user->role === 1) {
                DB::table('user_facility')->insert(['user' => $user->id, 'facility' => $request->get('facility')]);
            }


            if ((int)$user->role === 2) {
                DB::table('user_district')->insert(
                    [
                        'user' => $user->id,
                        'district' => Facility::find($request->get('facility'))->getDistrict->id //district
                    ]
                );
            }

        }

        if ($userSaved){

            $user->facilities;

            echo '<h1>Success</h1>';
            echo '<a href="/register">Add more users/facilities</a>';
            echo '<pre>',print_r($user->toJson(JSON_PRETTY_PRINT)),'</pre>';
        }

    }

    public function assignFac(Request $request) {

        $userId = $request->get('userId');

        $query = 'insert into user_facility(user, facility) values ( ?,?)';

        DB::insert($query, [$userId, $request->get('facility')]);

        return redirect('users/'.$userId.'/uaf');

    }

    public function assignDis(Request $request) {

        $userId = $request->get('userId');

        $query = 'insert into user_district(user, district) values ( ?,?)';

        DB::insert($query, [$request->get('userId'), $request->get('district')]);

        return redirect('users/'.$userId.'/uaf');

    }

    public function removeUserFac(Request $request) {

        $t = ''.$request->get('type');

        if ( $t === '0') {

            DB::table('user_facility')
                ->where('user', $request->get('userId'))
                ->where('facility', $request->get('facility'))
                ->delete();

        } else {


            DB::table('user_district')
                ->where('user', $request->get('userId'))
                ->where('district', $request->get('facility'))
                ->delete();

        }

        return 0;
    }


}
