<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OAuth2Middleware;
use App\User;
use Illuminate\Support\Facades\DB;
use Response;

class ApiController extends Controller {


    public function getHighRiskHumanCases(User $user) {


        if ($user->role != User::ROLE_VET) return [];

        $query = "
                select DISTINCT 
                       H.SURNAME,
                       H.OTHER_NAMES,
                       H.POPULAR_NAME,
                       H.USERNAME,
                       H.`_URI`,
                       H.`_CREATION_DATE`,
                        u.fullname as 'sender',
                       f.name as 'facilityName'
                
                from odk.HUMAN_FORM_CORE H
                            left join ses.users as u on u.id = H.USERNAME
                            left join ses.user_facility as uf on uf.user = u.id
                            left join ses.facilities as f on f.id = uf.facility 
                            left join ses.VW_HIGH_RISK_CASES as hrc on hrc.`_URI` = H.`_URI` 
                
                where f.district = ? and hrc.`_URI` is not null 
                order by H.`_CREATION_DATE` desc
                ";

        //TODO: make sure every vet has district.. otherwise crush
        $districtId = $user->districts[0]->id;

        $foo = DB::select($query, [$districtId]);


        $foo = collect($foo)->map(function($item) {
            $item->title = "Case ". strtoupper(explode('-', $item->_URI)[3]) . ": " . $item->facilityName;
            return $item;
        });


        return response($foo)->header("Cache-Control", "public, max-age=30");
    }

    public function getAllHumanCases(User $user) {


        if ($user->role != User::ROLE_VET) return [];

        //find vets district
        //then find all cases from that district..
        //Amen.

        $query = "
                select
                       H.SURNAME,
                       H.OTHER_NAMES,
                       H.POPULAR_NAME,
                       H.USERNAME,
                       H.`_URI`,
                       H.`_CREATION_DATE`,
                       u.fullname as 'sender',
                       f.name as 'facilityName'
                
                from odk.HUMAN_FORM_CORE H
                            left join ses.users as u on u.id = H.USERNAME
                            left join ses.user_facility as uf on uf.user = u.id
                            left join ses.facilities as f on f.id = uf.facility
                
                where f.district = ? order by H.`_CREATION_DATE` desc
                ";

        //TODO: make sure every vet has district.. otherwise crush
        $districtId = $user->districts[0]->id;

        $foo = DB::select($query, [$districtId]);

        $foo = collect($foo)->map(function($item) {
            $item->title = $item->SURNAME . ", " . $item->OTHER_NAMES;
            return $item;
        });

        return response($foo)->header("Cache-Control", "public, max-age=30");
    }

}
