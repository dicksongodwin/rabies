<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateFacilityRequest;
use App\Http\Requests\Admin\UpdateFacilityRequest;
use App\Repositories\Admin\FacilityRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FacilityController extends AppBaseController
{
    /** @var  FacilityRepository */
    private $facilityRepository;

    public function __construct(FacilityRepository $facilityRepo)
    {
        $this->facilityRepository = $facilityRepo;
    }

    /**
     * Display a listing of the Facility.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->facilityRepository->pushCriteria(new RequestCriteria($request));
        $facilities = $this->facilityRepository->all();

        return view('admin.facilities.index')
            ->with('facilities', $facilities);
    }

    /**
     * Show the form for creating a new Facility.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.facilities.create');
    }

    /**
     * Store a newly created Facility in storage.
     *
     * @param CreateFacilityRequest $request
     *
     * @return Response
     */
    public function store(CreateFacilityRequest $request)
    {
        $input = $request->all();

        $facility = $this->facilityRepository->create($input);

        Flash::success('Facility saved successfully.');

        return redirect(route('admin.facilities.index'));
    }

    /**
     * Display the specified Facility.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $facility = $this->facilityRepository->findWithoutFail($id);

        if (empty($facility)) {
            Flash::error('Facility not found');

            return redirect(route('admin.facilities.index'));
        }

        return view('admin.facilities.show')->with('facility', $facility);
    }

    /**
     * Show the form for editing the specified Facility.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $facility = $this->facilityRepository->findWithoutFail($id);

        if (empty($facility)) {
            Flash::error('Facility not found');

            return redirect(route('admin.facilities.index'));
        }

        return view('admin.facilities.edit')->with('facility', $facility);
    }

    /**
     * Update the specified Facility in storage.
     *
     * @param  int              $id
     * @param UpdateFacilityRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFacilityRequest $request)
    {
        $facility = $this->facilityRepository->findWithoutFail($id);

        if (empty($facility)) {
            Flash::error('Facility not found');

            return redirect(route('admin.facilities.index'));
        }

        $facility = $this->facilityRepository->update($request->all(), $id);

        Flash::success('Facility updated successfully.');

        return redirect(route('admin.facilities.index'));
    }

    /**
     * Remove the specified Facility from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $facility = $this->facilityRepository->findWithoutFail($id);

        if (empty($facility)) {
            Flash::error('Facility not found');

            return redirect(route('admin.facilities.index'));
        }

        $this->facilityRepository->delete($id);

        Flash::success('Facility deleted successfully.');

        return redirect(route('admin.facilities.index'));
    }
}
