<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OAuth2Middleware;
use App\Instance;
use App\Models\Admin\Facility;
use App\OdkHuman;
use App\OdkSample;
use App\OdkVet;
use App\OdkVet0;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class DataController extends Controller {
    public function __construct() {
        //todo: auth here
        $this->middleware('auth', ['except' => [
            'instanceUploaded',
            'getStats',
            'getGraphData',
            'viewInApp',
            'getHrcForVet'
        ]]);
    }

    public function listData(Request $request) {

        $formShortCode = $request->get('d');

        switch ($formShortCode) {

            case OdkHuman::getShortCode() :
                $title = 'Health Facility';
                $mapping = OdkHuman::getFormDataModelStatic();
                $query = OdkHuman::limit(1000000);
                break;

            case OdkVet0::getShortCode() :
                $title = 'VET v1 (*Deprecated)';
                $mapping = OdkVet0::getFormDataModelStatic();
                $query = OdkVet0::limit(1000000);
                break;

            case OdkVet::getShortCode() :
                $title = 'VET v2';
                $mapping = OdkVet::getFormDataModelStatic();
                $query = OdkVet::limit(1000000);
                break;

            case OdkSample::getShortCode() :
                $title = 'Sample Collection';
                $mapping = OdkSample::getFormDataModelStatic();
                $query = OdkSample::limit(1000000);
                break;


            default:
                echo 'Invalid request';
                return 0;

        }

        unset($mapping['USERNAME']);


        if ($request->has('u')) {
            $query = $query->where('USERNAME', $request->get('u'));
        }

        $data = $query->paginate(15)->appends(Input::except('page'));


        if (sizeof($data) === 0) {
            return $title. ': Data not available yet';
        }

        return view('data.data')
            ->with('data', $data)
            ->with('title', $title)
            ->with('formShortCode', $formShortCode)
            ->with('mapping', $mapping);
    }


    public function view($uri, Request $request) {

        $shortCode = $request->get('form');

        $i = null;

        if ($shortCode == OdkHuman::getShortCode()) $i = OdkHuman::find($uri);
        if ($shortCode == OdkVet0::getShortCode()) $i = OdkVet0::find($uri);
        if ($shortCode == OdkVet::getShortCode()) $i = OdkVet::find($uri);
        if ($shortCode == OdkSample::getShortCode()) $i = OdkSample::find($uri);


        if ($i) {
            $dm = $i->getFormDataModelStatic();
            return view('data.data-single')->with('data', $i)->with('dm', $dm);
        } else {
            return 'Could not find data';
        }

    }

    public static function makeTable($data, $headers, $link) {
        $data  = $data->toArray();

        if (sizeof($data) == 0) return;

        echo '<table class="table table-responsive" id="table-facs">';
        echo   '<thead>';

        echo '<tr>';
        foreach ($headers as $k => $v) {
            echo '<th style="text-transform: capitalize;" id="th_'.$k.'">'.$k.'</th>';
        }
        echo '</tr></thead>';

        echo '<tbody>';

        foreach($data as $datum) {
            echo '<tr>';
            foreach($headers as $k => $v) {
                if ($k === 'fullname') {
                    echo '<td>';
                    echo "<a href='/users/$datum[$link]/uaf'>$datum[$k]</a>";
                    echo '</td>';
                    continue;
                }
                echo '<td>';
                echo $datum[$k];
                echo '</td>';

            };
            echo '</tr>';
        };

        echo  '</tbody></table>';

    }

    public function getCases($uid) {
        $cases = User::find($uid)->cases;
        return $cases;
    }

    public function getStats() {
        return Response::json(
            [
                'vet' => OdkVet0::count() + OdkVet::count(),
                'human' => OdkHuman::count(),
                'sample' => OdkSample::count(),
            ]
        );
    }


    public function showData($title, $data) {

        $headers = collect(array_keys($data[0]->toArray()));
        $headers = $this->cleanHeaders($headers);

        return view('data')->with('data', $data)->with('title', $title)->with('headers', $headers);
    }


    /***
     * @param Request $request - userId
     * @return mixed
     *
     * This is called when an ODK instance has been uploaded
     * --> we do not know if it has been successfully saved or no.
     */
    public function instanceUploaded(Request $request) {

        $user = User::find($request->get('userId'));
        $meta = $request->get('instanceId');

        if (strpos($meta, 'uuid') === 0) {
            $instanceId = $meta;
            $formId = '';
        } else {
            $p = xml_parser_create();
            xml_parse_into_struct($p, $meta, $vals, $index);
            xml_parser_free($p);
            $instanceId = $vals[1]['value'];

            $pos = strrpos($meta, "</meta>");
            if ($pos === false) { // note: three equal signs
                $formId = "";
            } else {
                $formId = substr($meta, $pos + 9);
                $formId = substr($formId, 0, -1);
            }
        }

        $instance = new Instance();
        $instance->userId = $user->id;
        $instance->formId = $formId;
        $instance->uri = $instanceId;
        $saved = $instance->save();


        $fcm = [];
        $smsRes = [];

        if ($user && $user->role == User::ROLE_HW) {

            $model = OdkHuman::find($instance->uri);

            $vets = $user->vets();

            $fcm = NotificationManager::notifyOnCase($model, $vets);

            //check if is HRC, if true: $highRiskCase will be >= 0 depending on number categories it fits
            $highRiskCase = DB::table('VW_HIGH_RISK_CASES')->where('_URI', $instance->uri)->count();

            if ($highRiskCase > 0) {

                $vetPhones = [];
                foreach ($vets as $vet) {
                    array_push($vetPhones, $vet->phone);
                }

                $sms = $this->create_message($model);

                $data = [
                    "messages" => [
                        "from" => SmsController::SMS_SENDER_TITLE,
                        "to" => $vetPhones,
                        "text" => $sms
                    ]
                ];

                $smsRes = SmsController::sendMany($data);
            }

        }


        //todo: implement
//        SendFcmNotification::dispatch($instance, $user)->onQueue('fcm');
//        SendSmsNotification::dispatch($instance, $user)->onQueue('sms');

        return response()->json(['success' => $saved, 'fcm' => $fcm, 'sms' => $smsRes]);
    }

    private function create_message($model) {


        $name = $model->POPULAR_NAME . " " . $model->SURNAME . " (" . $model->OTHER_NAME . ")";

        $demographics = $model->AGE . ", " . $model->SEX;

        $village = ($model->LOCATION_OF_EVENT_VILLAGE == null) ? $model->LOCATION_OF_EVENT_VILLAGE : $model->VILLAGE_IF_NOT_LISTED;


        $ward = $model->LOCATION_OF_EVENT_WARD . " ward";

        // Telephone - including whether or not recorded
        $tel = ($model->PHONE_NO == null) ? $model->PHONE_NO : "(No phone number recorded)";

        // Relevant dates
        $bite_date = substr($model->GROUP_FIRST_VISIT_DATE_BITTEN, 1, 10);
        $visit_date = substr($model->VISIT_DATE, 1, 10);

        //# Biting animal
        $biting_animal = "The biting " . $model->GROUP_FIRST_VISIT_BITING_ANIMAL .
        ($model->GROUP_FIRST_VISIT_OWNER_NAME == null) ? " belonged to" . $model->GROUP_FIRST_VISIT_OWNER_NAME : " was unknown";

        //# ALERTS
        //# For a rabies patient
        if ($model->VISIT_STATUS == "positive_clinical_signs") {
            $sms = "ALERT: " . $name . " from " . $village . " in " . $ward .
                " " . $tel . " presented with signs of rabies on " . $visit_date . ". Please investigate!";
        } else {
            # And for a high risk bite
            $sms = "ALERT: a high risk bite to " . $name . " from " . $village . " in " . $ward .
                " " . $tel . " occurred on " . $bite_date . ". Please investigate!" . $biting_animal;
        }

        return $sms;
    }

    public function _view($uri) {

        $i = OdkVet::find($uri);
        if (!$i) $i = OdkHuman::find($uri);
        if (!$i) $i = OdkSample::find($uri);

        if ($i) {
            return view('data.data-single')->with('data', $i);
        } else {
            return 'Could not find data';
        }

    }

    public function viewInApp($uri) {

        $i = OdkVet::find($uri);
        if (!$i) $i = OdkHuman::find($uri);
        if (!$i) $i = OdkSample::find($uri);

        if ($i) {
            $mapping = $i->getFormDataModelStatic();
            return view('data.data-single-mobile')
                ->with('data', $i)
                ->with('mapping', $mapping);

        } else {
            return 'Could not find data';
        }

    }

    public function download() {

        $d = [
            'VW_HUMAN_FORM' => 'HF data',
            'VW_ANIMAL_FORM' =>  'VET data',
            'VW_ANIMAL_FORM_V2' =>  'VET v2 data',
            OdkSample::getTableName() => 'Sample Collection data'
        ];

        return view('data.data-download')->with('dataTypes', $d);

    }

    public function downloadSave(Request $request) {

        $table = $request->get('type');
        $startDate = $request->get('startDate');
        $endDate = $request->get('endDate');

        $startDate = Carbon::parse($startDate)->format('Y-m-d');
        $endDate = Carbon::parse($endDate)->format('Y-m-d');

        $res = DB::table($table)
            ->where('_CREATION_DATE', '>=', $startDate)
            ->where('_CREATION_DATE', '<=', $endDate)
            ->get()
            ->map(
                function($o) {
                    unset($o->_MARKED_AS_COMPLETE_DATE);
                    unset($o->_CREATOR_URI_USER);
                    unset($o->_LAST_UPDATE_URI_USER);
                    unset($o->_LAST_UPDATE_DATE);
                    unset($o->_UI_VERSION);
                    unset($o->_IS_COMPLETE);
                    unset($o->_MODEL_VERSION);
                    return $o;
                }
            );

        if (sizeof($res) === 0) {
            return "<p style='font-size: large; padding: 20px'>Dataset is empty for the chosen duration ($startDate - $endDate)" .
                " <br/> <a href='/data/download'>Go Back</a></p>";
        }


        $res = json_decode(json_encode($res), true);

        $filename = 'data_'.date('Ymd_His').'.csv';
        Storage::put($filename, '');
        $path = Storage::path($filename);
        $this->array_to_csv($res, $path);

        return Storage::download($filename);

    }

    private function array_to_csv($array, $filename, $delimiter=",") {
        if (sizeof($array) === 0) return;

        //todo: memory
        // open raw memory as file so no temp files needed, you might run out of memory though
        $f = fopen($filename, 'w');
        // loop over the input array

        $header = array_keys($array[0]);
        fputcsv($f, $header, $delimiter);


        foreach ($array as $line) {
            // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter);
        }
        // reset the file pointer to the start of the file
        fseek($f, 0);
        fclose($f);

    }


    public function getHighRiskHumanCases(User $user) {


        if ($user->role != User::ROLE_VET) return [];


        $query = "
                SELECT users.id 
                FROM users
                LEFT JOIN `user_facility` ON (`user_facility`.`user` = users.id)
                WHERE role = 1 
                AND `user_facility`.`facility` IN (
                    SELECT id FROM facilities WHERE district IN 
                        (SELECT district FROM `user_district` WHERE `user_district`.user = ?)
	            )";

        $foo = DB::select($query, [$user->id]);
        $foo = collect($foo)->map(function($item) { return $item->id; });

        $columns = [
            "_URI",
            "_CREATION_DATE",
            "GROUP_FIRST_VISIT_DATE_REPORTED_TO_HOSPITAL",
            "GROUP_FIRST_VISIT_OWNER_NAME",
            "GROUP_FIRST_VISIT_OWNER_RESIDENCE",
            "LOCATION_OF_EVENT_REGION",
            "COMMENTS",
            "VISIT_DATE",
            "PHONE_NO",
            "POPULAR_NAME",
            "VILLAGE_IF_NOT_LISTED",
            "USERNAME",
            "AGE",
            "SURNAME",
            "HUMAN_ID",
            "GROUP_FIRST_VISIT_BITING_ANIMAL",
        ];

        $uris = Instance::whereIn('userId', $foo)->where('formId', 'human_form')->select('uri')
            ->get()
            ->map(function($i) {
                return $i->uri;
            });

        //todo: select some columns
        $cases = OdkHuman::whereIn('_URI', $uris)
            ->select($columns)
            ->get()
            ->map(function($item) {


                $fname = Facility::inRandomOrder()->first()->name;

                $caseId = $item->_URI;
                $chunks = explode('-', $caseId);
                $caseId = $chunks[sizeof($chunks)-1];
                $caseId = strtoupper(substr($caseId, 0, 4));
                $caseId = "Case " . $caseId . ':'. $item->LOCATION_OF_EVENT_VILLAGE . "      " .$fname;


                $r = $item;
                $item->_CREATION_DATE = date('d M, h:i', strtotime($item->_CREATION_DATE));
                $item->_URI = $caseId;
                $r['sender'] = User::find($item->USERNAME);
                return $r;
            });

        return response($cases)->header("Cache-Control", "public, max-age=30");
    }

    public function getAllHumanCases(User $user) {


        if ($user->role != User::ROLE_VET) return [];


        $query = "
                select
                       H.SURNAME,
                       H.OTHER_NAMES,
                       H.POPULAR_NAME,
                       H.USERNAME,
                       H.`_URI`,
                       H.`_CREATION_DATE`,
                       u.fullname,
                       f.name
                
                from odk.human_form_core H
                            left join ses.users as u on u.id = H.USERNAME
                            left join ses.user_facility as uf on uf.user = u.id
                            left join ses.facilities as f on f.id = uf.facility
                
                where f.district = ?";


        $foo = DB::select($query, $user->districts[0]->id);

        return response($foo)->header("Cache-Control", "public, max-age=30");
    }

}
