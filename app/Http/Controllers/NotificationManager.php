<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 18-Aug-17
 * Time: 15:44
 */

namespace App\Http\Controllers;

use App\OdkModel;


class NotificationManager {

    public static function notifyOnCase(OdkModel $model, $users) {
        //find reviewers for that facility and send them a notification

        $tokens = [];
        foreach ($users as $u) array_push($tokens, $u->fcm_token);


        //edit here accordingly
        $data = array(
            'case_id' => $model->_URI,
            'category' => 'hw'
        );

        return self::sendPushNotification($data, $tokens, 'rabies');

    }


    // Payload data you want to send to Android device(s)
    // (it will be accessible via intent extras)


    // The recipient registration tokens for this notification
    // https://developer.android.com/google/gcm/

    //$ids = array('abc', 'def');

    // Send push notification via Google Cloud Messaging
    //sendPushNotification($data, $ids );

    public static function sendPushNotification($data, $ids, $project) {
        // Insert real GCM API key from the Google APIs Console
        // https://code.google.com/apis/console/


        $apiKey = env('FCM_SERVER_KEY_' . strtoupper($project));


        // Set POST request body
        $post = array(
            'registration_ids' => $ids,
            'data' => $data,
//            'notification'      => array(
//                'title' => 'Notification test ' . random_int(234,7484),
//                'body' => json_encode($data)
//            )
        );


        // Set CURL request headers
        $headers = array(
//            'Authorization: key=' . $apiKey,
            'Authorization: key=AIzaSyCmK_CIZoSyTgPqdbDk0lW_P-EPhMc1bf0',
            'Content-Type: application/json'
        );

        $response = [];

        // Initialize curl handle
        $ch = curl_init();

        //disable cert verification, u can be hacked...
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Set URL to GCM push endpoint
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set custom request headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set JSON post data
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));

        // Actually send the request
        $result = curl_exec($ch);

        // Handle errors
        if (curl_errno($ch)) {
            $response["curl error"] = json_decode(curl_error($ch), true);
        }

        // Close curl handle
        curl_close($ch);

        // Debug GCM response
        $response["curl result"] = json_decode($result, true);

        return $response;

    }

}


/***
 *
 *
 * {
 * "to": "foAVlzwD_V8:APA91bEvbpXej3wsUTIw4a_kcY2QFH0TvKrcVfHC0L-WUeoeCw7qpeUaxiLGe57OG_YGOs9IxU5sBTOavsUBESXODKNaNIMbOd6CGdSBrkdaV0koSOxszlevL7pY0oG18k3ifqrQ7Vd7",
 * "collapse_key": "type_a",
 * "notification": {
 * "body": "Notification: {% now 'iso-8601', '' %}",
 * "title": "Kennedy Mnyambo"
 * },
 * "data": {
 * "category": "hw",
 * "body": "First Notification",
 * "title": "Collapsing A",
 * "uri": "uuid:90794122-02cc-42bb-8a5e-7626a5949c21",
 * "case_id": "Hello"
 * }
 * }
 *
 *
 *
 *
 */


