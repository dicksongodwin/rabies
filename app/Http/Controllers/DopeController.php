<?php

namespace App\Http\Controllers;

use App\Http\Middleware\OAuth2Middleware;
use App\OdkHuman;
use App\OdkVet;
use App\OdkVet0;
use Illuminate\Support\Facades\DB;


class DopeController extends Controller {


    public function foo() {

        $finalResponse = [];

        $tables = [
            OdkHuman::getTableName(),
            OdkVet::getTableName(),
            OdkVet0::getTableName(),
            //OdkSample::getTableName()
        ];

        foreach ($tables as $table) {
            $query = "
            select r.USERNAME, i.userId, r.`_URI`, i.uri
            from odk.$table r
            left join ses.instances as i on r.`_URI` = i.uri
            where i.userId IS NOT NULL and (r.USERNAME <> i.userId  or r.USERNAME is null);
            ";

            $mismatchingUserId = DB::select($query);
            $count = sizeof($mismatchingUserId);

            if ($count == 0) {
                $finalResponse[$table] = [false, $count];
                continue;
            }

            $updateUrisWithCommas = implode(',', array_map( function($item) {return "'$item->uri'";}, $mismatchingUserId));

            $updateQuery =  "UPDATE $table SET `USERNAME` = CASE ";

            //dd($mismatchingUserId);

            foreach ($mismatchingUserId as $r) {
                $updateQuery .= "WHEN `_URI` = '$r->_URI' THEN $r->userId ";
            }

            $updateQuery .= " ELSE 0 END ";

            $updateQuery .= " WHERE `_URI` IN ($updateUrisWithCommas);";

            //dd($updateQuery);

            $o = DB::connection('odk')->statement($updateQuery);

            $finalResponse[$table] = [$o, $count];
        }



        return $finalResponse;

    }
}
