<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 16-Aug-17
 * Time: 15:48
 */

namespace App\Http\Controllers;


class Tokenizer {


    const TOKEN_LIFETIME = 300; //minutes
    const TOKEN_HASH_ALGORITHM = 'ripemd160';

    public static function verifyToken($token) {

        if ($token === null) {
            return self::error("Authentication failed, null token.");
        }

        $shite = explode(':', $token)[0];
        $expires_at = intval(explode('.', $shite)[2]);
        if (round(microtime(true)) > $expires_at) {
            return self::error('Authentication token expired');
        }

        $expected_token = self::hash($shite);
        if (!hash_equals($token, $expected_token)) {
            return self::error("Authentication failed, invalid token.");
        }

        //success, proceed
        return array('success' => true);
    }

    public static function generateToken($id) {
        $secret = bin2hex(random_bytes(8));
        $expire = round(microtime(true)) + (self::TOKEN_LIFETIME * 60);
        $shite = $id.'.'.$secret.'.'.$expire; //998.5004c185feea0513.1502879527129

        $token = self::hash($shite);

        return $token;
    }

    private static function error($message) {
        return  array(
            'success' => false,
            'error' => $message
        );
    }

    private static function hash($text) {
        return $text.':'.hash_hmac(self::TOKEN_HASH_ALGORITHM, $text, env('API_SECRET'));
    }


}