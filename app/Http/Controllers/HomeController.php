<?php

namespace App\Http\Controllers;

use App\Models\District;
use App\Models\Facility;
use App\User;
use Illuminate\Support\Facades\DB;
use Khill\Lavacharts\Lavacharts;


class HomeController extends Controller {
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {

        $data = DB::connection('odk')->select('CALL get_stats_rabies(1, 90)');
        $legendSuffix = ' submissions for last  '. sizeof($data)  .' days';

        $lava = new Lavacharts;

        $casesDataTable1 = $lava->DataTable()->addDateColumn('Date')->addNumberColumn('Animal' . $legendSuffix);
        $casesDataTable2 = $lava->DataTable()->addDateColumn('Date')->addNumberColumn('Human' . $legendSuffix);
        $casesDataTable3 = $lava->DataTable()->addDateColumn('Date')->addNumberColumn('Sample' . $legendSuffix);
        $casesDataTable4 = $lava->DataTable()->addDateColumn('Date')->addNumberColumn('Total' . $legendSuffix);

        $todayData = [
            'animal' => $data[0]->visits,
            'human' => $data[0]->obstetrics,
            'sample' => $data[0]->postnatal,
            'total' => $data[0]->visits + $data[0]->obstetrics + $data[0]->postnatal
        ];

        foreach ($data as $datum) {
            $casesDataTable1->addRow([$datum->dt,  $datum->visits]);
            $casesDataTable2->addRow([$datum->dt,  $datum->obstetrics]);
            $casesDataTable3->addRow([$datum->dt,  $datum->postnatal]);
            $casesDataTable4->addRow([$datum->dt,  $datum->postnatal + $datum->visits + $datum->obstetrics ]);
        }


        $options = [
            'legend' => 'bottom',
            'hAxis' => array(
                'baselineColor' => '#f2f2f2',
                'gridlineColor' => '#f0f2f2'
            ),
            'animation' => array(
                'duration' => 600,
                'startup' => true
            )
        ];

        $lava->LineChart('Cases1', $casesDataTable1, $options);
        $lava->LineChart('Cases2', $casesDataTable2, $options);
        $lava->LineChart('Cases3', $casesDataTable3, $options);
        $lava->LineChart('Cases4', $casesDataTable4, $options);

        return view('home')->with('lavaObject', $lava)->with('todayData', $todayData);
    }


    public function healthworkers()
    {
        $users = User::where('role','1')->get();
        return view('healthworkers')->with('users',$users);
    }
    public function fieldofficers(){
        $users = User::where('role','2')->get();
        return view('healthworkers')->with('users',$users);
    }

    public function chmt(){
        $users = User::where('role','>', 2)->where('role','<', 5)->get();
        return view('chmt')->with('users', $users);
    }

    public function facilities() {
        return view('facilities')->with('users', User::all())->with('facilities',Facility::all());
    }

    public function districts() {
        return view('districts')->with('districts', District::orderBy('district.region', 'asc')->get());
    }

    public function districtDetails($did) {
        return view('district_details')->with('d', District::find($did));
    }






}
