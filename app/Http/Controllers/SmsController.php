<?php

namespace App\Http\Controllers;

class SmsController extends Controller {

    const SMS_SENDER_TITLE = "SkyConnect";
    const TIMEOUT = 60;
    const MAXREDIRS = 10;

    public static function sendOne1($recipient, $message) {

        $recipient = self::formatPhone($recipient);

        $data = [
            "from" => self::SMS_SENDER_TITLE,
            "to" => $recipient,
            "text" => $message
        ];

        return self::sendOne($data);

    }

    private static function formatPhone($number) {
        //todo: better format on entry
        $number = preg_replace("/[^0-9]+/i", "", $number);

        if (strpos($number, "0") === 0) {
            $number = str_replace("0", "255", $number);
        }

        if (strpos($number, "255") !== 0) {
            $number = "255" . $number;
        }

        return $number;
    }

    private static function sendOne($data) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.infobip.com/sms/1/text/single",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => self::MAXREDIRS,
            CURLOPT_TIMEOUT => self::TIMEOUT,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
//                "authorization: Basic " . env('INFOBIP_KEY'),
                "authorization: Basic aWZha2FyYTpzbXNraW5n",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return [
            "response" => json_decode($response),
            "error" => json_decode($err)
        ];
    }

    public static function sendToHf($recipient, $message) {

        $recipient = self::formatPhone($recipient);

        $data = [
            "from" => self::SMS_SENDER_TITLE,
            "to" => $recipient,
            "text" => $message
        ];

        return self::sendOne($data);

    }

    public static function sendMany($data) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.infobip.com/sms/1/text/multi",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => self::MAXREDIRS,
            CURLOPT_TIMEOUT => self::TIMEOUT,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
//                "authorization: Basic " . env('INFOBIP_KEY'),
                "authorization: Basic aWZha2FyYTpzbXNraW5n",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return [
            "response" => json_decode($response),
            "error" => json_decode($err)
        ];
    }
}
