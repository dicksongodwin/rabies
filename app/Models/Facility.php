<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Facility extends Model {

    protected $table = 'facilities';
    public $timestamps = false;

    public function users() {
        return $this->belongsToMany(User::class, 'user_facility', 'facility', 'user');
    }

    public function getDistrict() {
        return $this->belongsTo(District::class, 'district');
    }


}
