<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{

    protected $table = 'district';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function getRegion() {
        return $this->belongsTo(Region::class, 'region');
    }

    public function facilities() {
        return $this->hasMany(Facility::class, 'district');
    }


    public function vets() {
        return $this
            ->belongsToMany(User::class, 'user_district', 'district', 'user')
            ->where('role', User::ROLE_VET);
    }
}
