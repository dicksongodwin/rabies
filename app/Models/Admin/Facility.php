<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model as Model;

/**
 * Class Facility
 * @package App\Models\Admin
 * @version August 21, 2018, 10:09 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection userDistrict
 * @property string name
 * @property integer district
 */
class Facility extends Model
{

    public $table = 'facilities';
    public $timestamps = false;



    public $fillable = [
        'name',
        'district'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'district' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    
}
