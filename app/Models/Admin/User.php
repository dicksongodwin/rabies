<?php

namespace App\Models\Admin;

use App\OdkHuman;
use App\OdkVet;
use App\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\District;
use App\Models\Facility;
use App\Models\FormData;
use App\Models\ProjectRoleForm;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function newQuery() {
        $query = parent::newQuery();
        return $query->orderBy('fullname', 'asc');
    }

    public function cases() {
        return $this->hasMany(FormData::class, 'creator_id');
    }

    public function facilities() {
        return $this->belongsToMany(Facility::class, 'user_facility', 'user', 'facility');
    }

    public function districts() {
        return $this->belongsToMany(District::class, 'user_district', 'user', 'district');
    }

    public function forms() {
        //SELECT form_id FROM `project_role_forms` WHERE project = 'rabies' AND role = 1
        return ProjectRoleForm::where('role', $this->role)
            ->select('form_id')
            ->get()
            ->map(function($item){
                return $item->form_id;
            });
    }

    public  function getDistrictAsFacilityHack() {
        $c = $this->districts;

        $c = $c->map(function($item) {
            $item->pivot = 0;
            $item->district = $item->id;
            return $item;

        });

        return $c;
    }

    public  function roleName() {
        return Role::find($this->role)->name;
    }

    public function totalCases() {
        return OdkHuman::where('USERNAME', $this->id)->count()+
                    OdkVet::where('USERNAME', $this->id)->count();
    }

    static $rules  = [''];

}
