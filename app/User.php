<?php

namespace App;

use App\Models\District;
use App\Models\Facility;
use App\Models\FormData;
use App\Models\ProjectRoleForm;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
    use Notifiable;

    const ROLE_HW = 1;
    const ROLE_VET = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'username', 'role'
    ];


    protected $hidden = [
        'password', 'remember_token', 'fcm_token'
    ];

    public function newQuery() {
        $query = parent::newQuery();
        return $query->orderBy('fullname', 'asc');
    }

    public function cases() {
        return $this->hasMany(FormData::class, 'creator_id');
    }

    public function facilities() {
        return $this->belongsToMany(Facility::class, 'user_facility', 'user', 'facility');
    }

    public function districts() {
        return $this->belongsToMany(District::class, 'user_district', 'user', 'district');
    }

    public function forms() {
        //SELECT form_id FROM `project_role_forms` WHERE project = 'rabies' AND role = 1
        return ProjectRoleForm::where('role', $this->role)
            ->select('form_id')
            ->get()
            ->map(function($item){
                return $item->form_id;
            });
    }

    public  function getDistrictAsFacilityHack() {
        $c = $this->districts;

        $c = $c->map(
            function($item) {
                $item->pivot = 0;
                $item->district = $item->id;
                return $item;
            }
        );

        return $c;
    }

    public  function roleName() {
        return Role::find($this->role)->name;
    }

    public function totalCases() {
        return
            OdkHuman::where('USERNAME', $this->id)->count() +
            OdkVet::where('USERNAME', $this->id)->count() ;
            //OdkSample::where('USERNAME', $this->id)->count(); -- no username in field list
    }


    public function isAdmin() {
        return $this->role == 5;
    }

    public function coworkers() {

        $coos = [];

        foreach ($this->facilities as $facility) {
            $coos = array_merge($coos, $facility->users->toArray());
        }

        return $coos;
    }

    public function vets() {

        if ($this->role == self::ROLE_VET) return [];

        //todo: enforce in DB that a single user with role 1 must have a single facility hence district
        $fs = $this->facilities;
        if (sizeof($fs) == 0) return []; //trivial

        $vets = $fs[0]->getDistrict->vets;

        return $vets;
    }

}
