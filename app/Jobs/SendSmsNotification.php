<?php

namespace App\Jobs;

use App\Instance;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSmsNotification implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $instance;
    protected $user;

    public function __construct(Instance $instance, User $user) {
        $this->instance = $instance;
        $this->user = $user;
    }


    public function handle() {

        //todo: check if in HRC [high risk cases]


    }
}
