<?php

namespace App\Jobs;

use App\Http\Controllers\NotificationManager;
use App\Instance;
use App\OdkHuman;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendFcmNotification implements ShouldQueue {
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $instance;
    protected $user;

    public function __construct(Instance $instance, User $user) {
        $this->instance = $instance;
        $this->user = $user;
        Log::info("Constructed");
    }


    public function handle() {
        Log::info("Handling...");

//
//        $instance = new Instance();
//        $instance->userId = $this->user->id;
//        $instance->formId = 0;
//        $instance->uri = 'test';
//
//        $saved = $instance->save();


        if ($this->user && $this->user->role == User::ROLE_HW) {

            $model = OdkHuman::find($this->instance->uri);

            $vets = $this->user->vets();

            NotificationManager::notifyOnCase($model, $vets);

        }

    }
}
