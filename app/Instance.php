<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instance extends Model {
    public $timestamps = false;
    public $incrementing = false;
    protected $primaryKey = 'uri';

    public function user() {
        return $this->belongsTo(User::class, 'userId');
    }
}
