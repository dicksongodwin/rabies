<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class OdkFormModel extends Model {
    protected $connection = 'odk';
    protected $table = '_form_data_model';
    public $timestamps = false;
    public $incrementing = false;


}
