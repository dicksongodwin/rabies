<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Facility;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FacilityRepository
 * @package App\Repositories\Admin
 * @version August 21, 2018, 10:09 am UTC
 *
 * @method Facility findWithoutFail($id, $columns = ['*'])
 * @method Facility find($id, $columns = ['*'])
 * @method Facility first($columns = ['*'])
*/
class FacilityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'district'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Facility::class;
    }
}
