<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/logout', 'UserController@logout')->name('logout'); //custom made logout


Route::get('/', function() {
    return redirect('/home');
});

Route::get('/home', 'HomeController@index');


Route::get('/hw', 'HomeController@healthworkers');
Route::get('/vet', 'HomeController@fieldofficers');
Route::get('/chmt', 'HomeController@chmt');

Route::get('/facilities', 'HomeController@facilities');
Route::get('/districts', 'HomeController@districts');


Route::get('/users/{id}', 'UserController@profile'); //for app
Route::get('/users/{id}/uaf', 'UserController@uaf');

Route::get('/f/view/{fid}', 'HomeController@fdetails');
Route::get('/d/view/{did}', 'HomeController@districtDetails');

Route::get('/register', 'UserController@register');
Route::post('/register-fac-submit', 'UserController@registerFacility')->middleware(\App\Http\Middleware\CheckAdmin::class);
Route::post('/register-submit', 'UserController@registerUser')->middleware(\App\Http\Middleware\CheckAdmin::class);

Route::post('/assign-fac', 'UserController@assignFac')->middleware(\App\Http\Middleware\CheckAdmin::class);
Route::post('/assign-dis', 'UserController@assignDis')->middleware(\App\Http\Middleware\CheckAdmin::class);
Route::post('/remove-user-fac', 'UserController@removeUserFac')->middleware(\App\Http\Middleware\CheckAdmin::class);
Route::post('/remove-user-dis', 'UserController@removeUserDis')->middleware(\App\Http\Middleware\CheckAdmin::class);

Route::get('/get/stats', 'DataController@getStats');

// admin
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin']], function () {
    Route::resource('facilities', 'FacilityController');
    Route::resource('users', 'UserController');
});

Route::post('user/fac', function (\Illuminate\Http\Request $request) {
    $error = '';

    if ($request->has('f')) {
        DB::table('user_facility')->insert(
            ['user' => $request->get('user'), 'facility' => $request->get('f')]
        );
    }

    if ($request->has('d')) {
        DB::table('user_district')->insert(
            ['user' => $request->get('user'), 'district' => $request->get('d')]
        );
    }

    return redirect()->back()->with('error', $error);
})->name('user_fac_shit');

//data
Route::group( ['prefix' => 'data', 'middleware' => ['auth', 'researcher']], function () {
    Route::get('/list', 'DataController@listData');
    Route::get('/download', 'DataController@download');
    Route::post('/download/save', 'DataController@downloadSave')->name('ds');
    Route::get('/view/{uri}', 'DataController@view');
});


Route::group( ['prefix' => 'data', 'middleware' => ['auth', 'researcher']], function () {
    Route::get('/list', 'DataController@listData');
//    Route::get('/download', 'DataController@download');
//    Route::post('/download/save', 'DataController@downloadSave')->name('ds');
    Route::get('/view/{uri}', 'DataController@view');
});

Route::get('/data/view-mobile/{uri}', 'DataController@viewInApp');

Route::get('/Vz9XYmvJGNr1k27e0erc', 'DopeController@foo');



