<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/cases/hrc/{user}', 'ApiController@getHighRiskHumanCases');
Route::get('/cases/human/all/{user}', 'ApiController@getAllHumanCases');

Route::post('/login', 'UserController@apiLogin');
Route::post('/updateFcmToken', 'UserController@updateFcmToken');

Route::post('/instanceUploaded', 'DataController@instanceUploaded');
