@extends('layouts.app')

@section('title', 'Facilities')

@section('content')

    <div class="row">

        <h2>List of districts</h2>
        <table id="table-facilities" class="table table-hover table-responsive table-condensed">
            <thead>
            <tr>
                <th>District</th>
                <th>Region</th>
                <th>Facilities</th>
                <th>LFO/VET</th>
            </tr>
            </thead>
            <tbody>

            @foreach($districts as $d)
                {{--<tr>--}}
                <tr onclick="window.location='/d/view/{{$d->id}}';">
                    <td>{{$d->name}}</td>
                    <td>{{$d->getRegion->name}}</td>
                    <td>
                        <ul>
                            @foreach($d->facilities as $f)
                                <li><a href="/f/view/{{$f->id}}">{{ $f->name }}</a></li>
                            @endforeach
                        </ul>
                    </td>
                    <td>
                        <ul>
                            @foreach($d->vets as $u)
                                <li><a href="users/{{$u->id}}/uaf">{{ $u->fullname }}</a></li>
                            @endforeach
                        </ul>
                    </td>
                </tr>

            @endforeach
            </tbody>

        </table>


    </div>

@endsection

@section('scripts')
    @parent

@endsection

@section('in-head')

    <style>

        td li {display: block;}
        td a {
            text-decoration: none;
            color: black;
        }

        td a:hover {
            text-decoration: underline;
            color: black;
        }

    </style>

@endsection