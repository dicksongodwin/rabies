@extends('layouts.app')


@section('title')
    Home
@endsection

@section('content')

    <div class="row">
        <total-data></total-data>
    </div>

    <div class="row today-data">
        <div class="col-md-3"> <p>Today <span>{{ $todayData['animal'] }}</span></p> </div>
        <div class="col-md-3"> <p> <span>{{ $todayData['human'] }}</span></p> </div>
        <div class="col-md-3"> <p> <span>{{ $todayData['sample'] }}</span></p> </div>
        <div class="col-md-3"> <p> <span>{{ $todayData['total'] }}</span></p> </div>
    </div>

    <div class="row">

        <div class="col-md-6"><div  id="lc1"></div></div>
        <?= $lavaObject->render('LineChart', 'Cases1', 'lc1') ?>

        <div class="col-md-6"><div  id="lc2"></div></div>
        <?= $lavaObject->render('LineChart', 'Cases2', 'lc2') ?>

    </div>

    <div class="row" style="margin-top: 12px;">

        <div class="col-md-6"><div  id="lc3"></div></div>
        <?= $lavaObject->render('LineChart', 'Cases3', 'lc3') ?>

        <div class="col-md-6"><div  id="lc4"></div></div>
        <?= $lavaObject->render('LineChart', 'Cases4', 'lc4') ?>

    </div>


@endsection

@section('scripts')
    @parent
@endsection

@section('in-head')
    <style type="text/css">
        .weight{
            font-weight: 600;
        }

        .today-data {
            background-color: #EEEEEE;
            border-bottom: 1px solid grey;
            border-top: 1px solid grey;
        }


        .today-data span {
            float: right;
            margin: 0 18px;
            font-weight: bolder;
            font-size: 1.2rem;
        }
    </style>

@endsection