@extends('layouts.app')

@section('content')
    <div class="container-fluid">


        <div class="row">
            <div class="col-md-12 col-xs-12 mb-4">

                <h2>{{ $d->name }}</h2>
                <h4> {{ $d->getRegion->name }}</h4>
                <hr>

                <?php
                use App\Http\Controllers\DataController;
                $vets = $d->vets;
                ?>


                <div class="row">

                    <div class="col-md-6">
                        @foreach($d->facilities as $f)

                            <div class="f-details" id="{{$f->id}}">

                                <h3>{{ $f->name }} ({{ sizeof($f->users)  }})</h3>
                                <?php
                                DataController::makeTable($f->users,
                                    ['fullname' => 'Fullname', 'username' => 'Username',
                                        'phone' => 'Phone number', 'desc' => 'Description'],
                                    'id'
                                )
                                ?>

                            </div>

                        @endforeach

                    </div>

                    <div class="col-md-1"></div>

                    <div class="col-md-5">
                        <h3>Vets/LFO</h3>
                        <?php
                        DataController::makeTable($d->vets,
                            ['fullname' => 'Fullname', 'username' => 'Username',
                                'phone' => 'Phone number', 'desc' => 'Description'],
                            'id'
                        )
                        ?>

                    </div>

                </div>


            </div>

        </div>

    </div>


@endsection

@section('scripts')
    @parent

@endsection

@section('in-head')

    <link rel="stylesheet" type="text/css" href="/css/main2.css?t={{random_int(34,223)}}">

@endsection