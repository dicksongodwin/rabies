@extends('layouts.mobile')

@section('title', 'Data')


@section('content')
    <div class="row" style="padding: 24px;">


        <h2>Data Details</h2>
        <h4><code>{{ $data->getUri()}}</code></h4>
        <h4>by <b>{{ $data->getDataSender() }}</b></h4>
        <hr>


        <div class="row">

            @foreach($mapping as $map_key => $map_value)

                <div class='field'>
                    <label>{{ $map_value }}</label>
                    <div class="field-value">{{ $data->$map_key }}</div>
                </div>

            @endforeach

        </div>

        {{-- todo:  add symptoms here.. --}}

    </div>

@endsection


@section('in-head')
    <style>

        .field {
            margin: 0;
            padding: 10px;
            border-bottom: 1px solid #c6c6c6;
        }

        .field label {
            font-size: smaller;
            font-family: Arial, "Roboto", sans-serif;
            margin: 0;
        }

        .field-value {
            font-size: large;
            font-family: "Roboto", sans-serif;
            color: #9e9e9e;
            text-transform: capitalize;
            margin: 0;
        }
    </style>
@endsection