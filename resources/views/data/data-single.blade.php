@extends('layouts.app')

@section('title', 'Data')

@section('side-nav')
    @parent
@endsection

@section('content')
    <div class="row" style="padding: 24px;">


        <h2>Data Details</h2>
        <h4><code>{{ $data->getUri()}}</code></h4>
        <h4>by <b>{{ $data->getDataSender() }}</b></h4>
        <hr>


        <div class="row">

            <?php
            $data = $data->toArray();

            $size = sizeof($data);

            $index = 0;
            $part = (int) ($size/3 - 3);

            echo "<div class='col-md-4'>";

            foreach ($data as $key => $value) {

                if (strpos($key, '_') === 0) {
                    continue;
                }

                if (strpos($key, 'META_INSTANCE_ID') === 0) {
                    continue;
                }

                if (strpos($key, 'USERNAME') === 0) {
                    continue;
                }

                $key = str_replace('GROUP_FIRST_VISIT', '', $key);
                $key = str_replace('OBSTETRIC_HISTORY', '', $key);
                $key = str_replace('PREGNANCY', '', $key);

                $key = str_replace('_', ' ', $key);

                $index++;
                echo "<div class='field'>";
                echo '<label>'.$key.'</label>';
                if (trim($value) === "") $value = "-";
                echo '<div class="field-value">'.$value.'</div>';
                echo '</div>';

                if ($index === $part) {
                    $index = 0;
                    echo "</div>";
                    echo "<div class='col-md-4'>";
                }
            }

            echo "<div class='col-md-4'>";


            ?>

        </div>

    </div>

@endsection


@section('in-head')
    <style>

        .field {  margin-bottom: 24px;  }

        .field label {
            font-size: smaller;
            margin: 0;
        }

        .field-value {
            font-size: large;
            font-family: "Roboto", sans-serif;
            color: #9e9e9e;
            text-transform: capitalize;
            margin: 0;
        }
    </style>
@endsection