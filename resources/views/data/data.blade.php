@extends('layouts.app')

@section('title', 'Data')

@section('side-nav')
    @parent
@endsection

@section('content')



    <div class="row" style="padding: 8px;">
        <h2> {{ $title  }}</h2>

        <div class="row" style="padding: 8px; background: #fafafa;">
            <div class="col-md-1" style="display:inline-block;font-size: 1.2rem; font-weight: bold;">
                <?php
                $start = 1 + ($data->currentPage() - 1 ) * $data->perPage();
                $end = $data->currentPage() * $data->perPage();
                if ($end > $data->total()) { $end = $data->total();}
                ?>
                {{ $start }} - {{ $end  }} of {{ $data->total() }}
            </div>
            <div class="col-md-12" style="display:inline-block;">{{ $data->links() }}</div>
        </div>

        <table class="table table-responsive table-striped">
            <thead>
            <tr>
                <th>SENDER</th>
                <th>DATE</th>

                @foreach($mapping as $key => $value)
                    <th>{{  str_replace("_", " ", $value) }}</th>
                @endforeach
            </tr>
            </thead>

            <tbody>
            @foreach( $data as $datum)
                <tr class="data" data-uri="{{ $datum->_URI }}" data-shortcode="{{ $formShortCode }}">
                    <td><a href="/user/{{ $datum->USERNAME or '#'}}/uaf"> {{$datum->getDataSender()}}</a></td>
                    <td>{{ \Carbon\Carbon::parse($datum->_CREATION_DATE)->format('d/m/Y H:i:s')}}</td>

                    @foreach($mapping as $key => $value)
                        <td>{{  $datum->$key }}</td>
                    @endforeach

                </tr>
            @endforeach

            </tbody>
        </table>

        {{ $data->links() }}


    </div>

@endsection


@section('scripts')
    @parent
    <script type="text/javascript" >

        $(document).ready(function () {

            $('tr.data').click(
                function () {
                    var uri = $(this).data("uri");
                    var shortCode = $(this).data("shortcode");
                    window.location = '/data/view/' + uri + "?form="+shortCode;
                }
            );
        });

    </script>
@endsection

@section('in-head')
    <style>

        td > a  {
            text-decoration: none;
            color: black;
        }
        td > a:hover {
            color: black;
        }

    </style>
@endsection