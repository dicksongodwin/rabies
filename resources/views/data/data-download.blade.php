@extends('layouts.app')

@section('title', 'Data')

@section('side-nav')
    @parent
@endsection

@section('content')
    <div class="row">

        <h2>Download</h2>
        <hr>

        <?php
            $now = date("d-m-Y");
            $nowMinus30 = date("d-m-Y",strtotime("-90 day"));
        ?>


        <div class="col-md-6" style="font-family: Roboto, sans-serif">

            {!! Form::open(['route' => 'ds']) !!}


            <div class="form-group col-sm-6">
                {!! Form::label('startDate', 'Start date') !!}
                {!! Form::text('startDate', $nowMinus30, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-6">
                {!! Form::label('endDate', 'End date') !!}
                {!! Form::text('endDate', $now, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-12">
                {!! Form::label('type', 'Data Type') !!}
                <br>
                {{ Form::select('type', $dataTypes,  ['class' => 'form-control']) }}
            </div>


            <!-- Submit Field -->
            <div class="form-group col-sm-12">
                {!! Form::submit('Download Data', ['class' => 'btn btn-primary']) !!}
            </div>


            {!! Form::close() !!}

        </div>

    </div>

@endsection


@section('in-head')

    <style>
        select {
            padding: 6px;
            background: radial-gradient(
                    ellipse at center,
                    rgba(255,255,255,0.9) 0%,
                    rgba(255,255,255,0.9) 31%,
                    rgba(255, 255, 255, 0.9) 50%,
                    #f1f1f1 100%
            );
        }
    </style>


    <script>
        $(document).ready(function(){

            console.log('every thing good!');

        });
    </script>

@endsection