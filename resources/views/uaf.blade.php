@extends('layouts.app')

@section('content')


    <div class="row">

        <h2>{{ $user->fullname  }}</h2>
        <h4>{{$user->phone}}, {{ $user->roleName()}}, {{ $user->desc or ''  }}</h4>

        <hr>
    </div>

    <div class="row">

        <div class="col-md-4 col-xs-12 mb-4">

            @if($user->role === 1 || $user->role === 2)
                <div class="round">
                    <span id="c-numbers"> {{ $user->totalCases() }}</span>
                    <p>Total cases submitted</p>
                </div>
            @endif

        </div>

        <div class="col-md-4 col-xs-12 mb-4">

            @if($user->role === 1)
                <h3>Facilities({{ sizeof($facilities) }})</h3>
                <ul>
                    @foreach ($facilities as $fac)
                        <li>
                            {{ $fac->name }}
                            <span class="li-fd hint" data-user=" {{ $user->id}}" data-fac="{{ $fac->id}}" data-fdt="0">Delete</span>
                        </li>
                    @endforeach
                </ul>
            @endif


            @if($user->role === 2)
                <h3>Districts({{ sizeof($districts) }})</h3>
                <ul>
                    @foreach ($districts as $fac)
                        <li>
                            {{ $fac->name }}
                            <span class="li-fd hint" data-user="{{ $user->id }}" data-fac="{{ $fac->id}}" data-fdt="1">Delete</span>
                        </li>
                    @endforeach
                </ul>
            @endif

        </div>


        <div class="col-md-4 col-xs-12 mb-4">
           <div id="chart"></div>
        </div>

    </div>


@endsection

@section('scripts')
    @parent

@endsection

@section('in-head')
    <link rel="stylesheet" type="text/css" href="/css/main2.css">
    <style>

        span.li-fd {
            visibility: hidden;
        }

        .border-left {
            border-left: 1px solid #d8d7d7;
            padding-left: 20px;
        }

        .round > span {
            display: block;
            height: 110px;
            width: 110px;
            line-height: 110px;

            border: 2px dotted gray;

            border-radius: 120px; /* or 50% */
            text-align: center;
            margin: 8px auto;
            font-size:1.7em;
        }

        .round {
            margin: 20px;
            text-align: center;
        }

        .fl li {
            margin-bottom: 4px;
        }
    </style>
@endsection