@extends('layouts.app')

@section('content')
    <div class="row">

        <div class="col-md-5">

            <form  method="post" action="/register-fac-submit">

                {{ csrf_field() }}


                <p class="e-logo">eSurveillance</p>

                <div class="row">
                    <div class="form-group-x">
                        <label for="fullname1">Facility name</label>
                        <input type="text"  placeholder="Eg Tarime DDH" id="fullname1"
                               name="fac" autocomplete="name" required>
                    </div>

                    <div class="form-group-x" id="div-select-fac-single1">
                        <label for="facility1">District</label>
                        <select class="form-control custom-select" id="facility1" name="districtId">
                            <?php
                            foreach($ds as $d) {
                                echo '<option value="'.$d->id.'">'.$d->name.'</option>';
                            }
                            ?>

                        </select>
                    </div>

                </div>

                <hr class="above-button">

                <button type="submit" class="btn btn-primary" style="margin: 0  4px 40px;">Add Facility</button>


            </form>
        </div>

        <div class="col-md-7">

            <form  method="post" action="/register-submit">

                {{ csrf_field() }}

                <p class="e-logo">eSurveillance</p>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group-x">
                            <label for="fullname">Fullname</label>
                            <input type="text"  placeholder="eg. Dr Maria John" id="fullname"
                                   name="fullname" autocomplete="name" required>
                        </div>

                        <div class="form-group-x">
                            <label for="username">Username</label>
                            <input type="text"  placeholder="Username" id="username" name="username" required>
                        </div>


                        <div class="form-group-x">
                            <label for="password">Password</label>
                            <!--                        todo: password-->
                            <input type="text" placeholder="Password" id="password" name="password" required>
                        </div>


                        <div class="form-group-x">
                            <label for="role">Role</label>
                            <select class="form-control custom-select" id="role" name="role">
                                <option value="1">HW</option>
                                <option value="2">LFO</option>
                            </select>
                        </div>


                    </div>
                    <div class="col-md-6">
                        <div class="form-group-x">
                            <label for="email">E-mail</label>
                            <input type="text"  placeholder="E-mail" id="email"
                                   autocomplete="email" name="email">
                        </div>

                        <div class="form-group-x">
                            <label for="fgi4">Phone number</label>
                            <input type="text"  id="fgi4" placeholder="Phone number"
                                   autocomplete="tel" name="phone"  required>
                        </div>


                        <div class="form-group-x" id="div-select-fac-single">
                            <label for="facility">Facility</label>
                            <select class="form-control custom-select" id="facility" name="facility">
                                <?php
                                foreach($facilities as $fac) {
                                    echo '<option value="'.$fac->id.'">'.$fac->name.'</option>';
                                }
                                ?>

                            </select>
                        </div>



                    </div>
                </div>

                <hr class="above-button">

                <button type="submit" class="btn btn-primary" style="margin: 0 auto 40px;">Add User</button>


            </form>

        </div>

    </div>
@endsection

@section('scripts')
    @parent

@endsection

@section('in-head')
    <link rel="stylesheet" href="{{ asset('css/register.css')  }}">

@endsection