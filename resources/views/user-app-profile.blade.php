
<html>
<head>
    <title>User Profile</title>

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <!-- bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript for bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    {{--<script src="https://unpkg.com/vue"></script>--}}

    <meta name="viewport" content="initial-scale=1, maximum-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

    <script type="text/javascript">
        $(window).load(function(){
            // PAGE IS FULLY LOADED
            // FADE OUT YOUR OVERLAYING DIV
            $('#loading').fadeOut();
        });
    </script>
    <style type="text/css">
        html, body {
            font-family: 'Open Sans', sans-serif;
            font-size: 17px;
            border-top: 4px solid rgba(169, 221, 201, 0.97);
            max-width: 100%;
            overflow-x: hidden;
        }

        .stats {
            padding: 18px;
        }

        #pname {
            font-size: 2.0rem;
            display: block;
        }
        .desc {
            margin: 0 8px;
            font-weight: 300;
            font-size: 0.99rem;
        }

        .fa {
            color: #4d4d4d;
            margin: 4px;
        }

        .st {
            text-align: center;
            font-weight: 200;
            font-size: 11.5vw;
        }

        .st > span {
            display: block;
            font-size: 1.2rem;
        }

        hr {
            color: #ddd;
            width: 88%;
            margin: 0;
        }

        h4 {
            font-size: 1.4rem;
            font-weight: 300;
        }

        .facilities {
            margin-left: 8px;
        }

        .facilities > p {
            font-size: 0.8rem;
            font-weight: 300;
            margin: 12px;
            border-left: 4px solid rgba(169, 221, 201, 0.97);
            padding-left: 8px;
            box-sizing: content-box;
        }

        ol li {
            font-weight: 300;
            display: inline-block;
            background: #ffffff;
            border: 1px solid #dedede;
            margin: 4px;
            padding: 4px;
        }

        ol li:hover {
            background: #f9f9f9;}

    </style>


</head>

<body>
<div class="container-fluid">

    <div class="row" id="loading" style="font-style: italic; font-weight: 300;">
        Loading...
    </div>


    <div class="col-sm-12 stats">
        <span id="pname">{{ $user->fullname }}</span>

        <p class="desc">
            <i class="fa fa-phone" aria-hidden="true"></i>
            {{ $user->phone2 or '-' }}
        </p>

        <p class="desc">
            <i class="fa fa-user-md" aria-hidden="true"></i>
            @if($user->role === 1)
                HW
            @else
                VET
            @endif
        </p>
    </div>

    <hr>

    <div class="row" style="margin: 20px auto">
        <div class="col-xs-6 st">
            <span>Submissions</span>
            {{ $user->totalCases() }}
        </div>

        {{--<div class="col-xs-6 st">--}}
            {{--<span>Accuracy</span>--}}
            {{--{{ 100 }}%--}}
        {{--</div>--}}
    </div>
    <hr>

    <div class="row" style="padding: 28px;">
        @if( sizeof($user->facilities))
            <h2>Facility</h2>
            @foreach($user->facilities as $facility)
                {{ $facility->name }}
            @endforeach
        @endif
    </div>
    

    <div class="row" style="padding: 28px;">
        @if( sizeof($user->districts))
            <h2>District</h2>
            @foreach($user->districts as $district)
                {{ $district->name }}
            @endforeach
        @endif
    </div>

    <div class="row lc">
        <div id="lc1"></div>
        <?= $lavaObject->render('LineChart', 'Cases1', 'lc1') ?>
    </div>




</div>

</body>
</html>
