@extends('layouts.app')

@section('title', 'Facilities')

@section('content')

<div class="row">

            <h2>List of Facilities</h2>
            <table id="table-facilities" class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>
                    <th>Facility</th>
                    <th>District</th>
                    <th>Region</th>
                </tr>
                </thead>
                <tbody>

                @foreach($facilities as $f)
                <tr onclick="window.location='/d/view/{{$f->getDistrict->id}}#{{$f->id}}';">
                  <td>{{$f->name}}</td>
                  <td>{{$f->getDistrict->name}}</td>
                  <td>{{$f->getDistrict->getRegion->name}}</td>
                </tr>

                @endforeach
                </tbody>

            </table>


</div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {

            $('table#table-facilities').DataTable({paging: false});
        });
    </script>


@endsection

@section('in-head')


@endsection