<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


@php
    $districts = \App\Models\District::all();
    $options = [];
    foreach ($districts as $d) $options[''.$d->id] = $d->name;
@endphp

<!-- District Field -->
<div class="form-group col-sm-6">
    {!! Form::label('district', 'District:') !!}
    {!! Form::select('district', $options, ['class' => 'form-control dope-select']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.facilities.index') !!}" class="btn btn-default">Cancel</a>
</div>
