<table class="table table-responsive" id="users-table">
    <thead>
    <tr>
        <th>Fullname</th>
        <th>Username</th>
        {{--<th>Email</th>--}}
        <th>Phone</th>
        {{--<th>Password</th>--}}
        <th>Role</th>
        <th>Desc</th>
        <th>Status</th>
        {{--<th>Remember Token</th>--}}
        {{--<th>Fcm Token</th>--}}
        <th colspan="3">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        <tr>
            <td>{!! $user->fullname !!}</td>
            <td>{!! $user->username !!}</td>
            {{--<td>{!! $user->email !!}</td>--}}
            <td>{!! $user->phone !!}</td>
            {{--<td>{!! $user->password !!}</td>--}}
            <td>{!! $user->role !!}</td>
            <td>{!! $user->desc !!}</td>
            <td>{!! $user->status !!}</td>
            {{--<td>{!! $user->remember_token !!}</td>--}}
            {{--<td>{!! $user->fcm_token !!}</td>--}}
            <td>
                {!! Form::open(['route' => ['admin.users.destroy', $user->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('admin.users.show', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('admin.users.edit', [$user->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>