@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            User
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">

                <div class="row">
                    {!! Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) !!}

                    @include('admin.users.fields')

                    {!! Form::close() !!}
                </div>


                <?php
                $cUser = \App\User::find($user->id);

                $fs = \App\Models\Facility::orderBy('name', 'desc')->get();
                $foptions = [];
                foreach ($fs as $f) $foptions[''.$f->id] = $f->name;

                $ds = \App\Models\District::orderBy('name', 'desc')->get();
                $doptions = [];
                foreach ($ds as $d) $doptions[''.$d->id] = $d->name;

                ?>

                <hr>

                <div class="row">
                    <div class="col-sm-6">
                        <h3>Facilities</h3>
                        <ul>
                            @foreach($cUser->facilities as $f1)
                                <li>{{ $f1->name }}</li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-sm-6">
                        <h3>Districts</h3>
                        <ul>
                            @foreach($cUser->districts as $d1)
                                <li>{{ $d1->name }}</li>
                            @endforeach
                        </ul>
                    </div>

                </div>

                <br><br>

                <hr>

                <div class="row">

                    <div class="col-md-6">

                    {!! Form::model($user, ['route' => 'user_fac_shit', 'method' => 'post']) !!}
                    <!-- Facility Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('f', 'Facility:') !!}
                            {!! Form::select('f', $foptions, ['class' => 'form-control']) !!}
                        </div>

                        <input type="hidden" name="user" value="{{$cUser->id}}">

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Add user to facility', ['class' => 'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}

                    </div>

                    <div class="col-md-6">&nbsp;

                    {!! Form::model($user, ['route' => 'user_fac_shit', 'method' => 'post']) !!}
                    <!-- District Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('d', 'District:') !!}
                            {!! Form::select('d', $doptions, ['class' => 'form-control']) !!}
                        </div>

                        <input type="hidden" name="user" value="{{$cUser->id}}">

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Add user to district', ['class' => 'btn btn-primary']) !!}
                        </div>

                        {!! Form::close() !!}

                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

