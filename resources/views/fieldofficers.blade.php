@extends('layouts.app')
@section('title', 'Vets')
@section('content')

<div class="row">


            <h2>List of Livestock Field Officers</h2>
            <table id="table-users" class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Fullname</th>
                    <th>Phone No</th>
                    <th>De</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr onclick="window.location='/users/{{$user->id}}/uaf';">
                <td>{{$user->id}}</td>
                <td>{{$user->fullname}}</td>
                <td>{{$user->phone}}</td>
                <td>
                    @foreach($user->facilities as $facility)
                    <span>{{$facility->name}}</span><br>
                    @endforeach
                </td>
                </tr>

                @endforeach
                </tbody>

            </table>

</div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {

            $('table#table-users').DataTable({paging: false});
        });
    </script>

@endsection

@section('in-head')


@endsection