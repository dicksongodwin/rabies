<div class="container-fluid">
    @php
        $user= \Illuminate\Support\Facades\Auth::user();
        $user_id = \Illuminate\Support\Facades\Auth::id();
    @endphp

    <div class="row" style="text-align: center; background: #fafafa;">
        <img src="http://dashboards.esurveillance.or.tz/images/profile_pic.svg" alt="Profile Picture" class="profile-pic-class link-profile">

        <div class="user-details">

            <span class="link-profile"> {{ $user->fullname }}</span>

            <hr style="width: 80%;">


            <a id="mp" href="/home">Home</a>|
            <a id="mp" href="#">My Profile</a>|
            <a id="mp" href="{{ route('logout') }}">Logout</a>
        </div>

    </div>

    <div class="row">
        <div class="list-group">

            <ul class="ul-menu">
                <li><a class= "list-group-item list-group-item-action" href="/hw">Health Workers</a></li>
                <li><a class= "list-group-item list-group-item-action" href="/vet">Field Officers</a></li>
                <li><a class= "list-group-item list-group-item-action" href="/facilities">Facilities</a></li>
                <li>
                    <a class= "list-group-item list-group-item-action" href="/districts">
                        Districts <span class="label label-primary">New!</span></a>
                </li>

                @if(Auth::user() && Auth::user()->isAdmin())
                    <li><a class= "list-group-item list-group-item-action" href="/register">Register</a></li>
                @endif

                @if(Auth::user() && Auth::user()->role >= 3)
                    <li><a class= "mt list-group-item list-group-item-action" href="#">Data</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/data/list?d={{\App\OdkHuman::getShortCode()}}">HF</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/data/list?d={{\App\OdkVet::getShortCode()}}">VET v2</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/data/list?d={{\App\OdkVet0::getShortCode()}}">VET v1</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/data/list?d={{\App\OdkSample::getShortCode()}}">Sample Collection</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/data/download">Download data</a></li>
                    @endif

                @if(Auth::user() && Auth::user()->isAdmin())
                    <li><a class= "mt list-group-item list-group-item-action" href="#">Admin</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/admin/users">Manage users</a></li>
                    <li><a class= "mo list-group-item list-group-item-action" href="/admin/facilities">Manage facilities</a></li>
                @endif

            </ul>

        </div>
    </div>



    <div class=" row signature">
        eSurveillance
    </div>

    <div class="below-menu-full-height" style="height: 50vh;">&nbsp;</div>

</div>
