@extends('layouts.app')
@section('title', 'HW')
@section('content')

<div class="row">


            <h2>List of Health Workers</h2>
            <table id="table-users" class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Fullname</th>
                    <th>Phone No</th>
                    <th>Facilities</th>
                    <th>Districts</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr onclick="window.location='/users/{{$user->id}}/uaf';">
                <td>{{$user->id}}</td>
                <td>{{$user->fullname}}</td>
                <td>{{$user->phone}}</td>
                <td>
                    @foreach($user->facilities as $facility)
                    <span>{{$facility->name}}</span> <span> ,</span>
                    @endforeach
                </td>
                    <td>
                    @foreach($user->districts as $d)
                    <span>{{$d->name}}</span> <span> ,</span>
                    @endforeach
                </td>
                </tr>

                @endforeach
                </tbody>

            </table>

</div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {

            $('table#table-users').DataTable({paging: false});
        });
    </script>

@endsection

@section('in-head')


@endsection