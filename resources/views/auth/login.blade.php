@extends('layouts.auth')

@section('title')
Rabies - Login
@endsection


@section('form')
<form class="form-horizontal" method="POST" action="/login">
            {{ csrf_field() }}

    <p class="e-logo">eSurveillance</p>
    <p class="login-project">Rabies login form</p>

    <input type="hidden" name="source" value="web">  <!-- for indicating where request came from-->

    <div class="form-group-x">
        <label for="username">Username</label>
        <input type="Username"  placeholder="Username" id="username" name="username" value="{{ old('username') }}" required>

    </div>
    @if ($errors->has('username'))
                <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                </span>
            @endif

    <div class="form-group-x">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" id="password" name="password" required>
    </div>

        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif


    <hr class="above-button">
    <button type="submit" class="btn btn-primary">SIGN IN</button>
<!--     <p class="do-have-account-or"><a href="/register">Sign Up.</a></p> -->

    @include('include/copyright');

</form>
@endsection