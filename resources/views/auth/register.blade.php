@extends('layouts.auth')

@section('title')
mHealth - Register
@endsection

@section('form')
<form class="form-horizontal register" method="POST" action="{{ route('register') }}">
    {{ csrf_field() }}

        <p class="e-logo">eSurveillance</p>
        <p class="login-project">mHealth registration form</p>

    <input type="hidden" name="source" value="web">  <!-- for indicating where request came from-->
<div class="row">
<div class="col-md-6">
    <div class="mr form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <label for="name">Name</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
    </div>
</div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username">Username</label>
                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 mr-2">
        <div class="mr form-group {{ $errors->has('phonenumber') ? ' has-error' : '' }}">
            <label for="phonenumber">Phonenumber</label>
                <input id="phonenumber" type="text" class="form-control" name="phonenumber" value="{{ old('username') }}" required autofocus>

                @if ($errors->has('phonenumber'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phonenumber') }}</strong>
                    </span>
                @endif
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email">E-Mail Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
        </div>
    </div>
</div>
<div class="row">
<div class="col-md-6">
    <div class="mr form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <label for="password">Password</label>
            <input id="password" type="password" class="form-control" name="password" required>

            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
    </div>
</div>
<div class="col-md-6">
    <div class="form-group">
        <label for="password-confirm">Confirm Password</label>
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>

    <hr class="above-button">
    <div class="center-div">
    <button type="submit" class="btn btn-primary">SIGN UP</button>
    <p class="do-have-account-or"><a href="/login">Sign in.</a></p>
    </div>

    @include('include/copyright');
</form>
@endsection
