@extends('layouts.app')
@section('title', 'Users')
@section('content')

<div class="row">

            <h2>List of Users</h2>
            <table id="table-users" class="table table-hover table-responsive table-condensed">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Fullname</th>
                    <th>View</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                <tr>
                  <td>{{$user->id}}</td>
                  <td>{{$user->fullname}}</td>
                  <td><a href="/users/{{$user->id}}/uaf">Details/Edit</a></td>
                </tr>

                @endforeach
                </tbody>

            </table>

</div>

@endsection

@section('scripts')
    @parent
    <script>
        $(document).ready(function () {
            $('table#table-users').DataTable({paging: false});
        });
    </script>

@endsection

@section('in-head')

@endsection