<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('title')</title>
    @include('include/header-links')
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

<div class="container-fluid">

    <div class="row">

        <div class="col-md-12 center-div">
            @yield('form')

        </div>
    </div>


</div>
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('css/login.css')  }}">

</body>
</html>