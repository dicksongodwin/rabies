<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rabies - @yield('title')</title>
    @include('include/header-links')
    @yield('in-head', '')
</head>

<body>
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top mj-nav" style="padding-top: 20px;">
    <div class="container-fluid">

        <div class="navbar-header">
            <a class="navbar-brand mj-title" href="/">eSurveillance</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right i-menu">
            </ul>
        </div>
    </div>
</nav>

<div class="main" id="app">
    <div class="main-row">

        <div class="main-left">
            @include('include/left-sidebar')
        </div>

        <div class="main-right">
            <div class="container-fluid" style="margin: 4px 20px;">
                @yield('content')
            </div>
        </div>

    </div>

</div>
<!-- Scripts -->

@section('scripts')
<script src="{{ asset('js/app.js') }}"></script>

  <!-- Latest compiled and minified JavaScript for bootstrap -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- for data tables  -->
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js" type="text/javascript"></script>

@show

</body>
</html>