<!DOCTYPE html>
<html lang="en">

<head>
    <title>Rabies - @yield('title')</title>
    @include('include/header-links')
    @yield('in-head', '')
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div class="main" id="app">

    <div class="container-fluid">
        @yield('content')
    </div>

</div>
<!-- Scripts -->

@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Latest compiled and minified JavaScript for bootstrap -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- for data tables  -->
    {{--<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.js" type="text/javascript"></script>--}}

@show

</body>
</html>